#!/usr/bin/env python3

class Flower:
    def __init__(self, color, smell):
        self.color = color
        self.smell = smell

    def describe_flower(self):
        print("This flower is {} and smells {}.".format(
            self.color, self.smell
        ))


color = input("What color is this flower? ")
smell = input("What is the smell? ")

flower = Flower(color, smell)
flower.describe_flower()
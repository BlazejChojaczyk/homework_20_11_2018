#!/usr/bin/env python3

name = input("What is your name? ")
surname = input("What is your surname? ")
age = input("How old are you? ")

print("Your name is: {}".format(name))
print("Your surname is: {}".format(surname))
print("You are {} years old".format(age))